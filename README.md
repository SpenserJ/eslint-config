# @vizworx/eslint-config

This package provides a common linting config used at VizworX, that is based upon [eslint-config-airbnb](https://npmjs.com/eslint-config-airbnb).

## Install

### npm

```sh
npx install-peerdeps --dev @vizworx/eslint-config
```

### Yarn 2:

```sh
$(yarn dlx install-peerdeps -Y --dev @vizworx/eslint-config --dry-run | grep 'yarn add' | sed 's/--registry .*//')
```

### Yarn 1:

```sh
npx install-peerdeps -Y --dev @vizworx/eslint-config
```

## Usage

### .eslintrc

```json
{
  "extends": "@vizworx/eslint-config"
}
```
